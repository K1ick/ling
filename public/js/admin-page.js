window.onload = function(){
	let counter_task = 2;
	let btn = document.getElementById("add-block").onclick = function(){
		addTask();
	};
	addAnime();
	function addAnime(){
		let arrSelect = document.querySelectorAll('.type-block select');
	let arrTaskContent = document.querySelectorAll(".task-block-content");
	let content1 = arrTaskContent[0].cloneNode(true);	
	let content2 = arrTaskContent[1].cloneNode(true);	

	for (el of arrSelect){
		el.onchange = function(){
			let task = this.parentElement.parentElement;
			let taskContent = task.querySelector(".task-block-content");

			console.log(taskContent);
			switch (this.value) {
				case '1':
					taskContent.innerHTML = '';
					taskContent.appendChild(content1);
					break
				case '2':
					taskContent.innerHTML = '';
					taskContent.appendChild(content2);

					break
			}
		}

	}
	}

	function addTask(){

		let form = document.getElementById("form");
		let taskBlocks = document.querySelector(".task-blocks");
		
		let taskBlock = document.querySelector(".task-block");
		console.log(taskBlock);
		// console.log(taskBlocks);
		let newTask = taskBlock.cloneNode(true);
		newTask.querySelector(".block-id").innerHTML = ++counter_task;
		taskBlocks.appendChild(newTask);
		addAnime();
	}

	document.getElementById('save').onclick = function(event){
		event.preventDefault();
		let arrTasks = document.querySelectorAll('.task-block');
		console.log(arrTasks);
		let data = {};
		let lessonTitle = document.getElementById('lesson-title').value;
		let course = document.getElementById('course').value;
		let obj = {
			type: 'head',
			course_id: course,
			title: lessonTitle
		}
		// data.push(obj);
		counter = 0;
		data[counter++] = obj;
		for (task of arrTasks) {
			let type = task.querySelector("select[name='type']").value;
			if (type == 1){
				let obj = {};
				obj.type = 'theory';
				obj.title = task.querySelector("input[name='theory-title']").value; // аголовок теории
				obj.content = task.querySelector("textarea").value; // текст теории
				// console.log(obj);
				data[counter++] = obj;
				// let theory_title = task.querySelector("input[name='theory_title']").value;

				// console.log(task_data);
			} else if (type == 2) {
				let obj = {};
				obj.type = 'task';
				obj.desc = task.querySelector("input[name='task-desc']").value; // описание задания
				obj.task = task.querySelector("input[name='task']").value; // предложение
				obj.right_ans = task.querySelector("input[name='right-ans']").value; // верный ответ
				obj.wrong_ans_1 = task.querySelector("input[name='wrong-ans-1']").value; // !верный ответ
				obj.wrong_ans_2 = task.querySelector("input[name='wrong-ans-2']").value; // !верный ответ
				obj.wrong_ans_3 = task.querySelector("input[name='wrong-ans-3']").value; // !верный ответ
				// console.log(obj);
				data[counter++] = obj;
			}

		}
		// let array = document.querySelectorAll("input, select, textarea");
		let headers = new Headers();
		headers.append("Content-Type", "application/json");
let requestInit = {
	method: 'POST',
	body: JSON.stringify(data),
	headers: headers,
	mode: 'same-origin',
	cache: 'default'
}
console.log(data);
console.log(JSON.stringify(data));
fetch('/add', requestInit)
.then(response => {
	console.log(response);
}) 
		// console.log(data);

		// for (el of array){
		// 	console.log(el.value);
		// }
	};
	// addTask();
}