<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatFirsttasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('firsttasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('course_id');
            $table->text('description');
            $table->text('text');
            $table->text('answer_id');
            $table->text('wrong_answer_id_1');
            $table->text('wrong_answer_id_2');
            $table->text('wrong_answer_id_3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('firsttasks');
    }
}
