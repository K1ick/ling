<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/course/{course_id}', function () {
    return view('course');
});
Route::get('/handler', 'CourseSender@index');

Route::get('/courses', 'Courses@index');
Route::get('/', function (){
    return view('main');
});

Route::post('/checkanswer', 'AnswerChecker@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
Route::get('/profile', function(){
    return view('profile');
});
Route::get('/edit', function(){
    return view('edit');
});
Route::post('/edit', 'EditController@index');
Route::get('/admin', 'AdminController@index');
Route::get('/admin/edit/$type/$data->id', 'AdminController@taskedit');
Route::get('/admin/add', 'AdminController@addtsk');
Route::post('/add', 'AdminController@add');