
@extends('layouts.app')
@section('content')
    <main class="main_content">
        <section class="main_content_section top-section">
            <div class="top-section_heading heading">
                <h1 class="heading_h1">Мы запускаем новый проект - <span class="heading_h1_youcourse">ЮКурсы</span></h1>
                <h2 class="heading_h2"><a href="/courses" class="heading_link start-link">Приступить</a></h2>
                <a href="https://youlang.ru/" target="_blank" class="heading_link ask-link">Что такое YouLang?</a>
            </div>
            <div class="top-section_map-wrapper">
                    <img class="top-section_map" src="{{asset('img/russia.svg')}}" alt="">
            </div>
        </section>
        <section class="main_content_section about-section">
            <div class="about-section_main-wrapper">
                <div class="about-section_text-wrapper">
                    <h3 class="about-section_h3">Lorem Ipsum</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam praesentium provident nihil deserunt illo porro inventore libero! Iste nobis, dolor exercitationem nesciunt non quos tempora quam dolore aperiam cumque qui.Fugit aliquam adipisci maxime nulla explicabo voluptate quae ipsa facere. Quas illo nihil consequatur recusandae. Laboriosam laborum ab animi vero recusandae, accusamus neque, excepturi officiis dolores, quidem ex eveniet in.</p>
                </div>
                    <img class="about-section_img" src="" alt="Image">
            </div>
        </section>
        <section class="main_content_section about-section">
                <div class="about-section_main-wrapper">
                    <img class="bout-section_img" src="" alt="Image">
                    <div class="about-section_text-wrapper">
                        <h3 class="about-section_h3">Lorem Ipsum</h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam praesentium provident nihil deserunt illo porro inventore libero! Iste nobis, dolor exercitationem nesciunt non quos tempora quam dolore aperiam cumque qui.Fugit aliquam adipisci maxime nulla explicabo voluptate quae ipsa facere. Quas illo nihil consequatur recusandae. Laboriosam laborum ab animi vero recusandae, accusamus neque, excepturi officiis dolores, quidem ex eveniet in.</p>
                    </div>
                </div>
        </section>
    </main>
@endsection
