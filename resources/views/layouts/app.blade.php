<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
        <!-- Styles -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/reset.css') }}" rel="stylesheet">
</head>
<body>
<header class="header">
    <div class="wrapper">
        <img src="{{asset('storage/logo.png')}}" alt="logo">
        <nav>
            <a href="/">Главная</a>
            <a href="/courses">Курсы</a>
            <a href="">Тренировки</a>
            <a href="">Словарь</a>
            @auth()
                @if(\Illuminate\Support\Facades\Auth::user()->isAdmin==1)
                    <a href="/admin">Админ панель</a>
                    @csrf
                @endif
                @endauth
        </nav>
        <ul>
            <!-- Authentication Links -->
            @guest
                <li>
                        <a href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                    <li>
                        <a href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                @endif
            @else
                <li>
                    <a href="/profile">
                        {{ \Illuminate\Support\Facades\Auth::user()->name }}
                    </a>

                    <div>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>
    </div>
</header>
<main class="main">
    @yield('content')
</main>
<section class="socials">
    <div class="wrapper">
        <div class="socials-list">
            <a href="">
                <object data="{{asset('storage/facebook.svg')}}" type="image/svg+xml">

                </object>
            </a>
            <a href="">
                <object data="{{asset('storage/inst.svg')}}" type="image/svg+xml">

                </object>
            </a>
            <a href="">
                <object data="{{asset('storage/vk.svg')}}" type="image/svg+xml">

                </object>
            </a>
        </div>
    </div>

</section>
<footer class="footer">
    <div class="wrapper">
        <nav>
            <a href="">О проекте</a>
            <a href="">Авторы</a>
            <a href="">Партнеры</a>
            <a href="">Отзывы</a>
            <a href="">Контакты</a>
        </nav>
        <span>Мобильное приложение: <a href="">IOS</a>, <a href="">Android</a></span>
    </div>

</footer>
</body>
</html>
