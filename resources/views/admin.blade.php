@extends('layouts.app')
@section('content')
    <nav class="admin-panel_menu sidebar">
        <ul class="admin-panel_menu_list menu_list">
            <li class="menu_list_item"><a class="menu_list_item_link users" href="/admin?func=us">Пользователи</a></li>
            <li class="menu_list_item"><a class="menu_list_item_link courses" href="/admin?func=st">Списки курсов</a></li>
        </ul>
    </nav>
    <section class="users-section">
        <div class="admin-sections-wrapper">
            <div class="users-section_heading">
                <h3 class="users-section_heading_h3">Пользователи</h3>
                <p class="users-section_heading_p">Список всех пользователей, включая администрацию.</p>
            </div>
            <ul class="users-section_list">
        @php
            if($func=='us'){
                foreach ($datas as $data){
                    if($data->isAdmin ==0)
                        echo " <li class='users-section_list_item'>
                    <div class='users-section_list_item_user-wrapper'>
                        <div>Id: <span>$data->id</span></div>
                        <div>Логин: <span>$data->name</span></div>
                        <div>Email: <span>$data->email</span></div>
                        <div>Админ: <span>$data->isAdmin</span></div>
                        <a class='users-section_list_item_user_make-admin' href='/admin?func=us&madm={$data->id}'>Сделать админом</a>
                    </div>
                </li>";
                    else
                        echo " <li class='users-section_list_item'>
                    <div class='users-section_list_item_user-wrapper'>
                        <div>Id: <span>$data->id</span></div>
                        <div>Логин: <span>$data->name</span></div>
                        <div>Email: <span>$data->email</span></div>
                        <div>Админ: <span>$data->isAdmin</span></div>
                        <a class='users-section_list_item_user_make-admin' href='/admin?func=us&dadm={$data->id}'>Снять администрирование</a>
                    </div>
                </li>";

                    }
                {{$datas->links();}}
            }
            else{
                foreach ($datas as $data){
                    echo " <li class='users-section_list_item'>
                    <div class='users-section_list_item_user-wrapper'>
                        <div>Курс <span>$data->id</span>: </div>
                        <div><span>$data->name</span></div>
                        <a class='users-section_list_item_user_make-admin' href='/admin/editcourse/$data->id'>Редактировать</a>
                    </div>
                </li>";
                    }
            }
        @endphp
            </ul>
        </div>
    </section>
    <section class="tasks-section hidden">
        <div class="admin-sections-wrapper">
            <ul>
                <li><a href="/admin?func=ex&type=1">Тип заданий 1</a></li>
                <li><a href="/admin?func=ex&type=2">Тип заданий 2</a></li>
                <li><a href="/admin?func=ex&type=3">Тип заданий 3</a></li>
                <li><a href="/admin?func=ex&type=4">Тип заданий 4</a></li>
            </ul>
        </div>
    </section>

@endsection