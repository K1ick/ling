@extends('layouts.app')
@section('content')
<main class="main">
    <div class="wrapper">
        <div class="course-list">
            @foreach ($courses as $course)
                <a href="/course/{{$course->id}}" class="course-list__item item">
                    <div class="item__cover" style="background-image: url({{asset($course->image)}})">

                    </div>
                    <div class="item__info">
                        <p>{{$course->name}}</p>
                    </div>
                </a>
            @endforeach


        </div>
    </div>

</main>
@endsection
