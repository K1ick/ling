@extends('layouts.app')

@section('content')
    <script type="text/javascript" src="{{\Illuminate\Support\Facades\URL::asset('js/admin-page.js')}}"></script>
    <div class="sidebar-add">
        <nav>
            <a class="courses active" href="#">Курсы</a>
            <a class="vocabulary" href="#">Словарь</a>
            <a class="stat" href="#">Статистика</a>
            <a class="users" href="#">Пользователи</a>
        </nav>
    </div>
    <div class="content">
        <div class="form-field">
            <form  method="POST" id="form">
                @csrf
                <div class="lesson-title">
                    <input type="text" name="title" id='lesson-title' required placeholder="Введите название урока">
                    <label >Курс: <select name='course' id="course">
                            <option value="1" selected> Творительный падеж</option>
                        </select>
                    </label>
                </div>
                <div class="task-blocks">
                    <div class="task-block" >
                        <div class="type-block">
                            Блок №<span class="block-id">1</span>:
                            <select name='type'>
                                <option value="1" selected>Теория</option>
                                <option value="2">Тест 1</option>
                                <option value="3">Тест 2</option>
                            </select>
                        </div>
                        <div class="task-block-content">
                            <input type="text" name="theory-title" placeholder="Заголовок теории">
                            <textarea name="content" cols="150" rows="15" placeholder="Добавьте теоретическую часть">Добавьте сюда теоретическую часть</textarea>
                        </div>
                    </div>
                    <div class="task-block">
                        <div class="type-block">
                            Блок №<span id="block-id">2</span>:
                            <select name='type'>
                                <option value="1" >Теория</option>
                                <option value="2" selected>Тест 1</option>
                                <option value="3">Тест 2</option>
                            </select>
                        </div>
                        <div class="task-block-content">
                            <div class="task-title">
                                <input type="text" name="task-desc" placeholder="Введите описание задания">
                                <input type="text" name="task" placeholder="Предложение">
                            </div>
                            <div class="answers">
                                <label> Верный ответ:
                                    <input type="text" name="right-ans">
                                </label>
                                <label> Неверный ответ 1:
                                    <input type="text" name="wrong-ans-1">
                                </label>
                                <label> Неверный ответ 2:
                                    <input type="text" name="wrong-ans-2">
                                </label>
                                <label> Неверный ответ 3:
                                    <input type="text" name="wrong-ans-3">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btns">
                    <a href="#" class="btn" id="add-block">Добавить блок</a>
                    <input type="submit" name="send" value="Сохранить" id="save">
                </div>
            </form>
        </div>
    </div>
@endsection