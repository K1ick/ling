<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class theory extends Model
{
    //
    protected $table = 'theory';
    public $primaryKey='id';
    public $incrementing=true;
}
