<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course extends Model
{
    protected $table ='courses';
    public $primaryKey='id';
    public $incrementing=true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasktype(){
        return $this->hasMany('App\tasktype', 'Task_type', 'content_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function theory(){
        return $this->hasOne('App\theory', 'id', 'content_id');
    }

}
