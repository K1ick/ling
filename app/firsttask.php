<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed answer_id
 */
class firsttask extends Model
{
    //
    protected $table = 'firsttasks';
    public $primaryKey='id';
    public $incrementing=true;
    public function RightAnswer(){
        return $this->answer_id;
        //return $this->hasOne('App\word', 'id', 'answer_id');
    }
    public function WrongAnswer_1(){
        return $this->wrong_answer_id_1;
       // return $this->hasOne('App\word', 'id', 'wrong_answer_id_1');
    }
    public function WrongAnswer_2(){
        return $this->wrong_answer_id_2;
        //return $this->hasOne('App\word', 'id', 'wrong_answer_id_2');
    }
    public function WrongAnswer_3(){
        return $this->wrong_answer_id_3;
        //return $this->hasOne('App\word', 'id', 'wrong_answer_id_3');
    }
}
