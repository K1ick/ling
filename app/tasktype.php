<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class tasktype extends Model
{
    protected $table='tasktypes';
    public $primaryKey='id';
    public $incrementing=true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function firsttask(){
        return $this->hasOne('App\firsttask', 'id', 'task_id');
    }
}
