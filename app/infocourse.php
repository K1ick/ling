<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/**
 * @method static find(int $int)
 */
class infocourse extends Model
{
    //
    protected $table ='infocourse';
    public $primaryKey='id';
    public $incrementing=true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function course(){
        return $this->hasMany('App\course', 'course_id', 'id');
    }
}
