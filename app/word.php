<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class word extends Model
{
    //
    protected $table='vocabulary';
    public $primaryKey='id';
    public $incrementing=true;
}
