<?php

namespace App\Http\Controllers;

use App\infocourse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class Courses extends Controller
{

    public function index()
    {
        $courses=infocourse::all();

        return view('courses', compact('courses'));
    }
}
