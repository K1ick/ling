<?php

namespace App\Http\Controllers;

use App\firsttask;
use App\fourthtasks;
use App\infocourse;
use App\secondtasks;
use App\thirdtasks;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        if (Auth::check()) {
            $func = $request->input('func');

            $type = $request->input('type');
            if (User::find(Auth::user()->getAuthIdentifier())->isAdmin == true) {
                if ($func) {
                    if ($func == 'us') {
                        if ($request->input('madm')) {
                            DB::table('users')
                                ->where('id', $request->input('madm'))
                                ->update(['isAdmin' => true]);
                            //error_log(serialize(User::find($request->input('madm'))));
                           // User::find($request->input('madm'))->update(['isAdmin' => 1]);

                        }
                        if ($request->input('dadm')) {
                            DB::table('users')
                                ->where('id', $request->input('dadm'))
                                ->update(['isAdmin' => false]);
                        }
                        $datas = User::paginate(20);
                        return view('admin', ['func' => $func, 'datas' => $datas]);
                    } else {
                       /* switch ($type) {
                            default:
                                $datas = firsttask::paginate(20);
                                $type = 1;
                                break;
                            case 1:
                                $datas = firsttask::paginate(20);
                                break;
                            case 2:
                                $datas = secondtasks::paginate(20);
                                break;
                            case 3:
                                $datas = thirdtasks::paginate(20);
                                break;
                            case 4:
                                $datas = fourthtasks::paginate(20);
                                break;
                        }
                        return view('admin', ['func' => $func, 'type' => $type, 'datas' => $datas]);
                    */  $datas=infocourse::paginate(20);
                        return view('admin', ['func' => $func, 'type' => $type, 'datas' => $datas]);}
                } else {
                    $datas = User::paginate(20);
                    $func = 'us';
                    return view('admin', ['func' => $func, 'datas' => $datas]);
                }
            } else
                return redirect('/courses');
        }
        else
            return redirect('/courses');
        }

        public function addtsk(){
        return view('adder');
        }


    /**
     * @param Request $request
     */
    public function add(Request $request)
    {
        $data = $request->getContent();
        $js = json_decode($data);

        error_log($js);
    }


}
