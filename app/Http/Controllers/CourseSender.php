<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json;
use Illuminate\Support\Facades\DB;
use App\course;
use App\tasktype;
use App\theory;
use App\firsttask;
use App\infocourse;
use Illuminate\Support\Facades\Log;

class CourseSender extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {

        $content=array();
        $infocourse=infocourse::find(1);
        $name=$infocourse->name;
        /* @var $infocourse \App\infocourse */

        $courses=$infocourse->course()->get();
        $ids=array();
        foreach ($courses as $course){

            $page_num=$course->page_num;

            if($course->content_type == 'theory'){
                $type='theory';
                /* @var $course \App\course */
                $theory=$course->theory()->get()->first();
                $description=$theory->description;
                $id=$theory->id;
                $text=$theory->theory_text;
                $b=['page_num'=>$page_num, 'type'=>$type, 'content'=>['description'=>$description, 'id'=>$id, 'text'=>$text]];
                array_push($content, $b);
            }
            elseif($course->content_type == 'task'){
                $type='task';
                /* @var $course \App\course */
                $tasks=$course->tasktype();
                $task=$tasks->inRandomOrder()->first();
                 switch ($task->Task_type){
                    default:
                        break;
                    case 1:
                        /** @var $task \App\tasktype */
                        $tsk=$task->firsttask()->first();
                        $tskid=$tsk->id;
                        $description=$tsk->description;

                        $text=$tsk->text;

                        $answ1=$tsk->RightAnswer();
                        $answ2=$tsk->WrongAnswer_1();

                        $answ3=$tsk->WrongAnswer_2();
                        $answ4=$tsk->WrongAnswer_3();
                        $answarr=array();
                        array_push($answarr, $answ1 );
                        array_push($answarr, $answ2 );
                        array_push($answarr, $answ3 );
                        array_push($answarr, $answ4 );
                        shuffle($answarr);
                        $right=array_search($answ1, $answarr);
                        $c=['page_num'=>$page_num, 'type'=>$type, 'content'=>['description'=>$description, 'type'=>$task->Task_type, 'id'=>$tskid, 'text'=>$text, 'ansArr'=>$answarr, 'right'=>$right]];
                        array_push($content, $c);
                        array_push($ids, $tskid);
                        break;
                }
            }

        }
        $arr=['name'=>$name, 'content'=>$content];



        return response()->json($arr);
    }

}
