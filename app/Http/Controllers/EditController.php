<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class EditController extends Controller
{
    public function index(Request $request){
        $input=Input::all();
        $user=Auth::user();
        $user->name=$input['name']
        Log::debug($user->name);
        $user->email=$input['email'];
        $user->save();
        return redirect('/profile');
    }
}
